#include<stdio.h>
#include<python3.5/Python.h>

//gcc -I/usr/include/python2.7 -lpython2.7 ctemp.c 
/*
-I/usr/include/python3.5/ -lpython3.5 -L/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/
*/
int main(){
    printf("IN C\n");
    Py_Initialize();
    if( !Py_IsInitialized()  ) {
        printf("Unable to initialize Python interpreter.");
        return 1;

    }
    printf("Prefix: %s\nExec Prefix: %s\nPython Path: %s\n",Py_GetPrefix(),Py_GetExecPrefix(),Py_GetProgramFullPath());
    printf("Module Path: %s\n",Py_GetPath());
    printf("Version: %s\nPlatform: %s\nCopyright: %s\n",Py_GetVersion(),Py_GetPlatform(),Py_GetCopyright());
    printf("Compiler String: %s\nBuild Info: %s\n",Py_GetCompiler(),Py_GetBuildInfo());

    PyObject *mymod=NULL,*myString=NULL;
    //mymod = PyString_FromString("pytemp");
    mymod = PyImport_ImportModule("pytemp.py");
    //PyRun_SimpleString("main");
    myString = PyObject_GetAttrString(mymod, "main");
    PyEval_CallObject()
    //printf("%s",myString);
    Py_Finalize();
    return 0;
}
