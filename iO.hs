--FOR INPUT LIKE
--5
--abc
--abcde
--abab
--aaa
--z
--AND OUTPUT IS LIKE
--bca cab abc
--bcdea cdeab deabc eabcd abcde
--baba abab baba abab
--aaa aaa aaa
--z
main = do
    n <- readLn :: IO Int
    input <- getContents
    let str = lines input
    --putStrLn $ show (map stringRotate str)
    --(\x -> putStrLn x) $ show (map stringRotate str)
    --mapM_ print (map stringRotate str)
    --mapM_ print (map (intercalate " ") (map stringRotate str))
    mapM_ putStrLn (map (intercalate " ") (map stringRotate str))
